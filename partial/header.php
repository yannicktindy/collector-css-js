<header>
    <h3>Menu</h3>
    <div class="dropdown">
        <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
            Simple
        </button>
        <ul class="dropdown-menu">
            <li><a class="dropdown-item" href="buttons.php">Buttons</a></li>
            <li><a class="dropdown-item" href="cards.php">Cards</a></li>
            <li><a class="dropdown-item" href="carousel.php">Carousel</a></li>
            <li><a class="dropdown-item" href="responsiveFont.php">Font Size responsive</a></li>
            <li><a class="dropdown-item" href="modal.php">Modal</a></li>
            <li><a class="dropdown-item" href="modal2.php">Modal2</a></li>
            <li><a class="dropdown-item" href="pallet1.php">Color Palette 1</a></li>
            
        </ul>

        <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
            complex
        </button>
        <ul class="dropdown-menu">
            <li><a class="dropdown-item" href="reverso.php">Reverso</a></li>

        </ul>

        <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
            Exit
        </button>
        <ul class="dropdown-menu">
            <li><a class="dropdown-item" href="../index.php">Collector Home</a></li>
            <li><a class="dropdown-item" href="https://silenus.fr">Retour CV</a></li>
        </ul>
    </div>

</header>
<hr>