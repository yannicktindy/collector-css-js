<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Description de votre page ici">
    <title>Silenus collector</title>
    <!-- link cdn bootstrap  -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../style.css">
    <title>Document</title>
</head>
<body>
<style>

    .box {
        width: 100%;
        display: flex;
        justify-content: center;
    }

    .flip-card {
        background-color: transparent;
        width: 300px;
        height: 300px;
        perspective: 1000px;
        border-radius: 10px;
        overflow: hidden;
        transform-style: preserve-3d;

    }

    .flip-card-inner {
        position: relative;
        width: 100%;
        height: 100%;
        transition: transform 0.6s;
        transform-style: preserve-3d;
    }

    /* .flip-card:hover .flip-card-inner {
        transform: rotateY(180deg);
        transform-style: preserve-3d;
    } */

    .flip-card-front, .flip-card-back {
        position: absolute;
        width: 100%;
        height: 100%;
        backface-visibility: hidden;
        border-radius: 10px;
        overflow: hidden;
    }

    .flip-card-front img, .flip-card-back img {
        width: 100%;
        border-radius: 10px;
        overflow: hidden;
        object-fit: cover;
        border-radius: 10px;   
    }

    .flip-card-front {
        background-color: black;
        color: black;
    }

    .flip-card-back {
        background-color: black;
        transform: rotateY(180deg);
    }


</style>

    <div class="colonne bg-dark p-3 p-sm-5">

    <?php 
    // Appel du template
    require_once '../partial/header.php';
    
    ?>
        <h1>Reverso</h1>
        <hr>

        <p>Flipping Card on click</p>
        <div class="box">
            <div class="flip-card">
                <div class="flip-card-inner">
                    <div class="flip-card-front">
                        <img src="../img/square/logo512.png" alt="dev">
                    </div>
                    <div class="flip-card-back">
                        <img src="../img/square/dev1.png" alt="dev">
                    </div>
                </div>
            </div>
           </div>
        <hr>


    </div>
 
    <!-- link cdn bootstrap  -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="../script.js"></script>
    <script>
        const flipCardInner = document.querySelector('.flip-card-inner');
        const flipCardFront = document.querySelector('.flip-card-front');
        const flipCardBack = document.querySelector('.flip-card-back');

        let isFlipped = false;

        flipCardInner.addEventListener('click', function(e) {
        if (isFlipped) {
            flipCardFront.style.zIndex = 2;
            flipCardBack.style.zIndex = 1;
            this.style.transform = 'rotateY(0deg)';
            isFlipped = false;
        } else {
            flipCardFront.style.zIndex = 1;
            flipCardBack.style.zIndex = 2;
            this.style.transform = 'rotateY(180deg)';
            isFlipped = true;
        }
        });

        // const flipCardBack = document.querySelector('.flip-card-back');

        // flipCardBack.addEventListener('click', function(e) {
        // e.stopPropagation(); // empêche l'événement click de se propager aux éléments parents
        // this.closest('.flip-card-inner').style.transform = 'rotateY(180deg)';
        // });

        // const flipCardInner = document.querySelector('.flip-card-inner');

    </script>
</body>
</html>