<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Description de votre page ici">
    <title>Silenus collector</title>
    <!-- link cdn bootstrap  -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../style.css">
    <title>Document</title>
</head>
<body>
<style>

    </style>

    <div class="colonne bg-dark p-3 p-sm-5">
          
        <?php 
        // Appel du template
        require_once '../partial/header.php';
        
        ?>
        <h1>Color palllet 1</h1>  
        <hr>

        <div class="p-3 bg-dark">
            <p style="color: #98C1D9">Primary: #98C1D9</p>
            <p style="color: #052F5F">Secondary: #052F5F</p>
            <p style="color: #2B7A78">Success: #2B7A78</p>
            <p style="color: #FFB400">Warning: #FFB400</p>
            <p style="color: #D90429">Danger: #D90429</p>
        </div>
        <hr>
        <div class="p-3 bg-light">
            <p style="color: #98C1D9">Primary: #98C1D9</p>
            <p style="color: #052F5F">Secondary: #052F5F</p>
            <p style="color: #2B7A78">Success: #2B7A78</p>
            <p style="color: #FFB400">Warning: #FFB400</p>
            <p style="color: #D90429">Danger: #D90429</p>
        </div>
        <hr>

        Primary: linear-gradient(to bottom, #8FBCCA, #5D9FBA)
        Secondary: linear-gradient(to bottom, #3C7693, #214F6E)
        Success: linear-gradient(to bottom, #69AEB2, #3E8C91)
        Warning: linear-gradient(to bottom, #FFD347, #FFB400)


    </div>
 
    <!-- link cdn bootstrap  -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="../script.js"></script>
    <script>

    </script>
</body>
</html>