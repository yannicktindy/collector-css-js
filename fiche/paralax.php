<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Description de votre page ici">
    <title>Silenus collector</title>
    <!-- link cdn bootstrap  -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../style.css">
    <title>Document</title>
</head>
<body>
    <style>
        .parallax {
        /* The image used */
        background-image: url("img_parallax.jpg");

        /* Set a specific height */
        min-height: 500px; 
        /* padding-top: 66.66%; */
         /* 3:2 Aspect Ratio (divide 2 by 3 = 0.6666)  */
        /* padding-top: 56.25%;  */
        /* 16:9 Aspect Ratio (divide 9 by 16 = 0.5625) */

        /* Create the parallax scrolling effect */
        background-attachment: fixed;
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
        }
    </style>

    <div class="colonne bg-dark p-3 p-sm-5">
          
        <?php 
        // Appel du template
        require_once '../partial/header.php';
        ?>
        <h1>paralax</h1>  
    <hr>
    
    <div class="parallax"></div>

    <div style="height:1000px;background-color:red;font-size:36px">
    Scroll Up and Down this page to see the parallax scrolling effect.
    This div is just here to enable scrolling.
    Tip: Try to remove the background-attachment property to remove the scrolling effect.
    </div>
 

    </div>
 
    <!-- link cdn bootstrap  -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="../script.js"></script>
    <script>


    </script>
</body>
</html>