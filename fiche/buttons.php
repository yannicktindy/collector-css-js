<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Description de votre page ici">
    <title>Silenus collector</title>
    <!-- link cdn bootstrap  -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../style.css">
    <title>Document</title>
</head>
<body>
<style>
        .butn {
            display: inline-block;
            background-image: linear-gradient(to bottom, #106a37, #0c8b3d);
            color: white;
            border: 2px solid #4CAF50;
            padding: 6px 12px;
            font-size: 16px;
            cursor: pointer;
            transition-duration: 0.4s;
            border-radius: 8px;
        }

        .butn:hover {
            transform: scale(1.1);
            box-shadow: 0 0 20px 10px rgba(16, 106, 55, 0.7);
        }

        .butn-out {
            display: inline-block;
            background-color: transparent;
            color: #4CAF50;
            border: 2px solid #4CAF50;
            padding: 6px 12px;
            font-size: 16px;
            cursor: pointer;
            transition-duration: 0.4s;
            border-radius: 8px;
            
        }

        .butn-anim {
            animation: border-animate 2s infinite;
        }

        .butn-out:hover {
            background-image: linear-gradient(to bottom, #106a37, #0c8b3d);
            color: #fff;
        }

        @keyframes border-animate {
            0% {
                border-color: #106a37;
            }
            50% {
                border-color: #0c8b3d;
            }
            100% {
                border-color: #106a37;
            }
        }

        .butn-auto-glow {
            /* position: relative; */
            animation: glow 2s ease-in-out infinite;
            background-image: linear-gradient(to bottom, #106a37, #0c8b3d);
            color: #fff;
        }

        @keyframes glow {
            0% {
                transform: scale(1);
                box-shadow: 0 0 0 0 rgba(16, 106, 55, 0.7);
            }
            50% {
                transform: scale(1.1);
                box-shadow: 0 0 40px 20px rgba(16, 106, 55, 0);
            }
            100% {
                transform: scale(1);
                box-shadow: 0 0 0 0 rgba(16, 106, 55, 0.7);
            }
        }

        .butn-glow {
            display: inline-block;
            background-image: linear-gradient(to bottom, #106a37, #0c8b3d);
            color: white;
            border: 2px solid #4CAF50;
            padding: 6px 12px;
            font-size: 16px;
            cursor: pointer;
            transition-duration: 0.4s;
            border-radius: 8px;
            transform: scale(1.1);
            box-shadow: 0 0 20px 10px rgba(16, 106, 55, 0.7);

        }

        /* Apply the gradient to the .glowing class as well */
        /* .butn-glow {
            background-image: linear-gradient(to bottom, #106a37, #0c8b3d);
            color: #fff;
        } */
    </style>

    <div class="colonne bg-dark p-3 p-sm-5">
          
        <?php 
        // Appel du template
        require_once '../partial/header.php';
        ?>
        <h1>Buttons Collection</h1>  
    <hr>

    <p>Buttons cloud animation, randomly glowing</p>
        <div class="butn-out butn-cloud">butn 1</div>
        <div class="butn-out butn-cloud">butn 2</div>
        <div class="butn-out butn-cloud">butn 3</div>
        <div class="butn-out butn-cloud">butn 4</div>
    <hr>

    <p>Button animation and glowing</p>
        <div class="butn butn-auto-glow">My Button</div>
    <hr>
    
    <p>Simple Button</p>
        <div class="butn">My Button</div>
    <hr>

    <p>Simple Button outline</p>
        <div class="butn-out">My Button</div>
    <hr>

    <p>Simple Button border anim</p>
        <div class="butn-out butn-anim">My Button</div>
    <hr>





    </div>
 
    <!-- link cdn bootstrap  -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="../script.js"></script>
    <script>
          // Select all buttons with the "butn-cloud" class
        const buttons = document.querySelectorAll('.butn-cloud');

        // Define a function to randomly replace a button's class with "butn-glow" for 1 second
        function highlightRandomButton() {
        // Select a random button from the list of buttons
        const randomButton = buttons[Math.floor(Math.random() * buttons.length)];
        // Replace the "butn-out" class with "butn-glow" on the selected button
        randomButton.classList.replace('butn-out', 'butn-glow');
        // Remove the "butn-glow" class after 2 seconds
        setTimeout(() => {
            randomButton.classList.replace('butn-glow', 'butn-out');
        }, 2000);
        }

        // Call the highlightRandomButton function every 1 second
        setInterval(highlightRandomButton, 2000);

    </script>
</body>
</html>