<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Description de votre page ici">
    <title>Silenus collector</title>
    <!-- link cdn bootstrap  -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../style.css">
    <title>Document</title>
</head>
<body>
    <style>
    /* The Modal (background) */
    .modal-overlay {
        display: none;
        position: fixed;
        z-index: 1;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        overflow: auto;
        /* Change the background-color to black to make it visible */
        background-color: rgba(0,0,0,0.4);
    }

    /* Modal Content/Box */
    .modal-content {
        background-color: #fff; /* Change the background color of the modal */
        margin: 15% auto;
        padding: 20px;
        width: 80%;
        max-width: 600px;
        /* Définit la position initiale de la modal */
        transform: translateY(-100%);
        /* Définit la durée et la fonction d'animation */
        animation: slide-in 0.6s ease-in-out forwards;
    }

    /* Ajoute la classe "active" pour afficher la modal */
    .modal-content.active {
        /* Déplace la modal vers le bas pour l'afficher */
        animation: slide-out 0.6s ease-in-out forwards;
    }

    /* Ajoute une transition pour la fermeture de la modal */
    .modal-overlay.active {
        /* Définit la durée et la fonction d'animation */
        transition: opacity 0.3s ease-in-out, transform 0.3s ease-out;

        /* Change l'opacité de l'overlay pour le rendre visible */
        opacity: 1;
        display: block; /* Add this to make the overlay visible */
    }

    /* Ajoute une transition pour la fermeture de la modal */
    .modal-content.active + .modal-overlay.active {
        /* Change l'opacité de l'overlay pour le rendre invisible */
        opacity: 0;
        display: none; /* Add this to hide the overlay */
    }

    /* Keyframes for slide-in animation */
    @keyframes slide-in {
        0% {
            transform: translateY(-100%);
        }
        100% {
            transform: translateY(0);
        }
    }

    /* Keyframes for slide-out animation */
    @keyframes slide-out {
        0% {
            transform: translateY(0);
        }
        100% {
            transform: translateY(-100%);
        }
    }
    </style>

    <div class="colonne bg-dark p-3 p-sm-5">

        <?php 
        // Appel du template
        require_once '../partial/header.php';
        ?>

        <h1>Simple modal 2</h1>  

        <div class="modal-overlay" id="modal1">
            <div class="modal-content">

                <h2>Modal Title</h2>
                <p>Modal content goes here.</p>
                <button class="close-modal btn btn-outline-success" data-close="#modal1">close</button>
            </div>
        </div>

        <button class="btn btn-outline-success" data-open="#modal1">Open Modal</button>
        <hr>

    </div>
 
    <!-- link cdn bootstrap  -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="../script.js"></script>
    <script>
            const openModalButtons = document.querySelectorAll('[data-open]');
    const closeModalButtons = document.querySelectorAll('[data-close]');
    const overlay = document.querySelector('.modal-overlay');

    openModalButtons.forEach(button => {
        button.addEventListener('click', () => {
            const modal = document.querySelector(button.dataset.open);
            openModal(modal);
        });
    });

    closeModalButtons.forEach(button => {
        button.addEventListener('click', () => {
            const modal = document.querySelector(button.dataset.close);
            closeModal(modal);
        });
    });

    function openModal(modal) {
        if (modal == null) return;
        modal.classList.add('active');
        overlay.classList.add('active');
    }

    function closeModal(modal) {
        if (modal == null) return;
        modal.classList.remove('active');
        overlay.classList.remove('active');
    }
    </script>
</body>
</html>