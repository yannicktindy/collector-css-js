<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Description de votre page ici">
    <title>Silenus collector</title>
    <!-- link cdn bootstrap  -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../style.css">
    <title>Document</title>
</head>
<body>
    <style>
        .text1 {
            font-size: max(2.5vw, 1.5rem)
        }
        .text2 {
            font-size: max(2vw, 1rem)
        }
        .text3 {
            font-size: max(1.5vw, 0.8rem)
        }

        .pad1 {
            padding: max(4vw, 2rem)
        }
        .pad2 {
            padding: max(2vw, 1rem)
        }
        .pad3 {
            padding: max(1vw, 0.5rem)
        }
        .mar1 {
            margin: max(4vw, 2rem)
        }
        .mar2 {
            margin: max(2vw, 1rem)
        }
        .mar3 {
            margin: max(1vw, 0.5rem)
        }
        .inner {
            width: 100%;
            
        }

    </style>

    <div class="colonne bg-dark p-3 p-sm-5">
          
        <?php 
        // Appel du template
        require_once '../partial/header.php';
        ?>
        <h1>Responsive font size on vw</h1>  
    <hr>
    <p>Simple Button</p>
        <div class="text1">Test de font size : class = text1</div>
        <div class="text2">Test de font size : class = text2</div>
        <div class="text3">Test de font size : class = text3</div>
    <hr>
    <div class="box bg-secondary mar1 pad1">
        <div  class="inner bg-black">
            mar1 pad1
        </div>
    </div>
    <div class="box bg-secondary mar2 pad2">
        <div  class="inner bg-black">
            mar2 pad2
        </div>
    </div>
    <div class="box bg-secondary mar3 pad3">
        <div  class="inner bg-black">
            mar3 pad3
        </div>
    </div>

 

    </div>
 
    <!-- link cdn bootstrap  -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="../script.js"></script>
    <script>


    </script>
</body>
</html>